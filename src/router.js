const Router = require('koa-router'),
  router = new Router(),
  model = require('./rhinoceros');

// Task 2a: create validation constraints
const Validator = require('jsonschema').Validator;
const V = new Validator;
const schema = {
  'required': [
    'name',
    'species',
  ],
  'properties': {
    'name': {
      'type': 'string',
      'minLength': 1,
      'maxLength': 20,
    },
    'species': {
      'type': 'string',
      'enum': [
        'white_rhinoceros',
        'black_rhinoceros',
        'indian_rhinoceros',
        'javan_rhinoceros',
        'sumatran_rhinoceros',
      ],
    },
  },
  "additionalProperties": false
};

// Task 1: get single rhino from data
router.get('/rhinoceros/:id', (ctx, next) => {
  const rhinoceros = model.getOne(ctx.params['id']);
  ctx.response.body = { rhinoceros };
});

// Task 2b: validate rhino before POSTing
router.post('/rhinoceros', (ctx, next) => {
  let validationResult = V.validate(ctx.request.body, schema); // using jsonschema to validate
  if(validationResult.errors.length === 0) { // if no errors, rhino is clean
    ctx.response.body = model.newRhinoceros(ctx.request.body);
  } else {
    return ctx.status = 422;
  };
});

// Task 3: allow for filtering of index using query strings
router.get('/rhinoceros', (ctx, next) => {
  const rhinoceroses = model.getAll(ctx.query); // ctx.query to read filters
  ctx.response.body = { rhinoceroses };
});

// Task 4: get all endangered rhinos
router.get('/rhinoceros/endangered', (ctx, next) => {
  const endangeredRhinoceroses = model.getEndangered();
  ctx.response.body = { endangeredRhinoceroses };
});

module.exports = router;
