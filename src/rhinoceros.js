const uuidv4 = require('uuid/v4');
const _ = require('lodash');
let rhinoceroses = require('./data');

// Task 3: filter out rhinos based on query string inputs
// n*m runtime; n = rhinos, m = included query strings
exports.getAll = (options) => {
  if(_.isEmpty(options)) {
    return rhinoceroses; // if no filters, return all rhinos
  } else {
    let optionKeys = Object.keys(options);
    return rhinoceroses.filter((rhino) => {
      if(optionKeys.every((key) => rhino[key] === options[key])) return true;
    });
  };
};

// Task 4: count each rhino using counter hash; return those where count <= 2
// 2n runtime (approaches n); n = rhinos
exports.getEndangered = () => {
  const rhinoCount = {};
  rhinoceroses.forEach((rhino) => {
    if(rhinoCount[rhino.species]) rhinoCount[rhino.species]++;
    else rhinoCount[rhino.species] = 1;
  });
  return rhinoceroses.filter((rhino) => rhinoCount[rhino.species] <= 2);
};

// Task 1: return rhino with matching id
exports.getOne = (id) => {
  return rhinoceroses.find((rhino) => rhino.id === id);
};

exports.newRhinoceros = data => {
  const newRhino = {
    id: uuidv4(),
    name: data.name,
    species: data.species
  };
  rhinoceroses.push(newRhino);
  return newRhino;
};
